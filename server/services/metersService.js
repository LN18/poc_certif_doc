const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = 'mongodb://localhost:27017/blockchaindb';
var ObjectId = require('mongodb').ObjectID;

class MetersService{
    constructor(req, res){
        this.req = req
        this.res = res
    }

    insert(meter, db, callback){
        db.collection('meters').insertOne({
            "meter" : meter
        }, function(){
            callback()
        })
    }

    update(query, db, callback){
        console.log(query);
        db.collection('meters').updateOne({_id:ObjectId(query._id)}, {$set: {meter:query.meter}}
    , function(){
            callback()
        })
    }

    getAllMeters(){
        let self = this;
        try{
            MongoClient.connect(url, function(err, db) {
                assert.equal(null, err);
                let users = []
                let cursor = db.collection('meters').find();

                cursor.each(function(err, doc) {
                    assert.equal(err, null);
                    if (doc != null) {
                        users.push(doc)
                    } else {
                        return self.res.status(200).json({
                            data: users
                        })
                    }
                });
            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }

    addMeter() {
        let self = this;
        let meter = this.req.body.meter;
        try{
            MongoClient.connect(url, function(err, db) {
                assert.equal(null, err);
                self.insert(meter, db, function(){
                    db.close()
                    return self.res.status(200).json({
                        status: 'success'
                    })
                })
            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }

    updateMeter() {
        let self = this;
        let meter = this.req.body.query;
        try{
            MongoClient.connect(url, function(err, db) {
                assert.equal(null, err);
                self.update(meter, db, function(){
                    db.close()
                    return self.res.status(200).json({
                        status: 'success'
                    })
                })
            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }
}
module.exports = MetersService
