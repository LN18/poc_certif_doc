const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const url = 'mongodb://localhost:27017/blockchaindb';

class AuthService{
    constructor(req, res){
        this.req = req
        this.res = res
    }

    insert(credentials, db, callback){
        db.collection('user').insertOne({
            "credentials" : credentials
        }, function(){
            callback()
        })
    }


    getCredential(){
        let self = this;
        let query = this.req.body.query;
        try{
            MongoClient.connect(url, function(err, db) {
                db.collection("user").find(query).toArray(function(err, result) {
                    if (err) throw err;
                    if(result.length!==0){
                        return self.res.status(200).json({
                            status: 'success',
                            data: result
                        })
                    }else{
                        return self.res.status(500).json({
                            status: 'error',
                            error: error
                        })
                    }


                });

            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }

    getAllUsers(){
        let self = this;
        try{
            MongoClient.connect(url, function(err, db) {
                assert.equal(null, err);
                let users = []
                let cursor = db.collection('user').find();

                cursor.each(function(err, doc) {
                    assert.equal(err, null);
                    if (doc != null) {
                        users.push(doc)
                    } else {
                        return self.res.status(200).json({
                            data: users
                        })
                    }
                });
            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }

    addCredentials() {
        let self = this;
        let credentials = this.req.body.credentials;
        try{
            MongoClient.connect(url, function(err, db) {
                assert.equal(null, err);
                self.insert(credentials, db, function(){
                    db.close()
                    return self.res.status(200).json({
                        status: 'success'
                    })
                })
            });
        }
        catch(error){
            return self.res.status(500).json({
                status: 'error',
                error: error
            })
        }
    }
}
module.exports = AuthService
