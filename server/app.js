const express = require('express')
const authService  = require('./services/authentificationService')
const metersService  = require('./services/metersService')
const app = express()
const bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended : false}))

//Authentification
app.get('/', function (req, res) {
    res.send('Welcome to Grocery Service APIs.')
})

app.post('/api/addCredentials', function (req, res) {
    let authServiceObj = new authService(req, res);
    authServiceObj.addCredentials();
})


app.post('/api/getCredentials', function (req, res) {
    let authServiceObj = new authService(req, res);
    authServiceObj.getCredential();
})

app.get('/api/getAllUsers', function (req, res) {
    console.log('toto');
    let authServiceObj = new authService(req, res);
    authServiceObj.getAllUsers();
})

//Smart Meters

app.post('/api/addMeter', function (req, res) {
    let meterServiceObj = new metersService(req, res);
    meterServiceObj.addMeter();
})

app.post('/api/updateMeter', function (req, res) {
    let meterServiceObj = new metersService(req, res);
    meterServiceObj.updateMeter();
})


app.get('/api/getAllMeters', function (req, res) {
    let meterServiceObj = new metersService(req, res);
    meterServiceObj.getAllMeters();
})

app.listen(3000, function () {
    console.log('Web app service listening on port 3000!')
})
