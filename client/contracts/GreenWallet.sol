pragma solidity ^0.4.18;

import "./ConvertLib.sol";

contract Wallet {

  address public addressWallet;


  function Wallet() public {
    addressWallet = msg.sender;
  }
}

contract GreenWallet {

  mapping(bytes32 => address) public identifiedWallets;
  mapping(bytes32 => uint) public electroCoinBalance;
  mapping(bytes32 => bytes32) public documentsHashes;
  address[] wallets;
  bytes32[] identifiers;
  bytes32[] hashes;
  uint[] transfers;

  function createWallet(bytes32 identifier) public {
    Wallet wallet = new Wallet();
    identifiedWallets[identifier] = address(wallet);
    electroCoinBalance[identifier] = 0;
    identifiers.push(identifier);
    wallets.push(address(wallet));
  }

  function getWallets() public view returns (address[]){
    return (wallets);
  }

  function getIdentifiers() public view returns (bytes32[]){
    return (identifiers);
  }

  function getTransfers() public view returns (uint[]){
    return (transfers);
  }

  function getOneWallet(bytes32 identifiant) public view returns (bytes32, address){
    return (identifiant, identifiedWallets[identifiant]);
  }

  function getCoinBalance(bytes32 identifiant) public view returns (bytes32, uint){
    return (identifiant, electroCoinBalance[identifiant]);
  }


  function transfer(bytes32 fromIdentifier, bytes32 toIdentifier, uint amount) public {
    require(electroCoinBalance[fromIdentifier] > amount);
    electroCoinBalance[fromIdentifier] -= amount;
    electroCoinBalance[toIdentifier] += amount;

  }

  function credit(bytes32 fromIdentifier, uint amount) public {
    electroCoinBalance[fromIdentifier] += amount;

  }

  function saveDocumentHash(bytes32 hash) public {
    hashes.push(hash);
    documentsHashes[hash] = 'edf';
  }

  function getDocumentByHash(bytes32 hash) public view returns (bool) {
    if (documentsHashes[hash] == 'edf') {
      return true;
    }
    return false;
  }

  function getHashes() public view returns (bytes32[]){
    return (hashes);
  }

  function getDiscount(bytes32 identifier, uint amount) public {
    require(electroCoinBalance[identifier] > amount);
    electroCoinBalance[identifier] -= amount;
  }

  function destroy() public {
    wallets = new Wallet[](0);
  }

}
