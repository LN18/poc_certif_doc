pragma solidity ^0.4.18;

contract DocumentCertification {
  mapping(bytes32 => bytes32) public documentsHashes;
  bytes32[] hashes;

  function saveDocumentHash(bytes32 hash) public {
    hashes.push(hash);
    documentsHashes[hash] = 'edf';
  }

  function getDocumentByHash(bytes32 hash) public view returns (bool) {
    if (documentsHashes[hash] == 'edf') {
      return true;
    }
    return false;
  }

  function getHashes() public view returns (bytes32[]){
    return (hashes);
  }

}
