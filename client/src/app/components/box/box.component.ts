import {Component,OnInit} from '@angular/core';
import {Ng4FilesService,Ng4FilesConfig,Ng4FilesStatus,Ng4FilesSelected} from 'angular4-files-upload';

declare interface RouteInfo {
  icon: string;
}

@Component({
  selector: 'app-box',
  templateUrl: './box.component.html',
  styleUrls: ['./box.component.css'],
})
//https://alligator.io/angular/httpclient-intro/ (loadbar: listen to progress)
//https://stackoverflow.com/questions/47034375/angular-file-upload-progress-percentage
export class BoxComponent  {
  public selectedFiles;

  private configImage: Ng4FilesConfig = {
      acceptExtensions: ['jpg', 'jpeg'],
      maxFilesCount: 5,
      totalFilesSize: 101200000
    };

  private configVideo: Ng4FilesConfig = {
      acceptExtensions: ['mp4', 'avi'],
      maxFilesCount: 1
    };

  constructor(
        private ng4FilesService: Ng4FilesService
    ) {}

    ngOnInit() {
      this.ng4FilesService.addConfig(this.configImage, 'my-image-config');
      this.ng4FilesService.addConfig(this.configVideo, 'my-video-config');
    }

    public filesSelect(selectedFiles: Ng4FilesSelected): void {
      if (selectedFiles.status !== Ng4FilesStatus.STATUS_SUCCESS) {
        this.selectedFiles = selectedFiles.status;
        return;
      }

      // Handle error statuses here

      this.selectedFiles = Array.from(selectedFiles.files).map(file => file.name);
    }

  }
