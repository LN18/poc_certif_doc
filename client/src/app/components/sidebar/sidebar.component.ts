import {Component, OnInit} from '@angular/core';

declare const $: any;

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  {path: 'home', title: 'Home', icon: 'home', class: ''},
  {path: 'auth', title: 'User', icon: 'person', class: ''},
  {path: 'files', title: 'Fichiers', icon: 'insert_drive_file', class: ''},
  {path: 'transfer', title: 'Historique', icon: 'history', class: ''},
  {path: 'exit', title: 'Log out', icon: 'exit_to_app', class: ''},
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }

  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
