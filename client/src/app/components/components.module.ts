import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DragulaModule } from 'ng2-dragula';
import { Ng4FilesModule } from 'angular4-files-upload';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { BoxComponent } from './box/box.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    DragulaModule,
    Ng4FilesModule
  ],
  declarations: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    BoxComponent
  ],
  exports: [
    FooterComponent,
    NavbarComponent,
    SidebarComponent,
    BoxComponent
  ]
})
export class ComponentsModule { }
