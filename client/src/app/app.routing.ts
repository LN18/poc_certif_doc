import {NgModule} from '@angular/core';
import {CommonModule,} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {Routes, RouterModule} from '@angular/router';

import {HomeComponent} from './home/home.component';
import {FilesComponent} from './files/files.component';
import {UserProfileComponent} from './user-profile/user-profile.component';
import {ExitComponent} from './exit/exit.component';
import {TableListComponent} from './table-list/smart-meters/table-list.component';
import {TypographyComponent} from './typography/typography.component';
import {IconsComponent} from './icons/icons.component';
import {MapsComponent} from './maps/maps.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {UpgradeComponent} from './upgrade/upgrade.component';
import {AuthComponent} from './auth/auth.component';
import {RegisterComponent} from "./auth/register.component";
import {TransferComponent} from "./transfer/transfer.component";
import {AdminComponent} from "./table-list/admin/admin.component";

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'files', component: FilesComponent},
  {path: 'auth', component: AuthComponent},
  {path: 'exit', component: ExitComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'transfer', component: TransferComponent},
  {path: 'user-profile', component: UserProfileComponent},
  {path: 'table-list', component: TableListComponent},
  {path: 'admin', component: AdminComponent},
  {path: 'typography', component: TypographyComponent},
  {path: 'icons', component: IconsComponent},
  {path: 'maps', component: MapsComponent},
  {path: 'notifications', component: NotificationsComponent},
  {path: 'upgrade', component: UpgradeComponent},
  {path: '', redirectTo: 'home', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [],
})
export class AppRoutingModule {
}
