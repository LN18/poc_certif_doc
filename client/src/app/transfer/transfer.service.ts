import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Http} from '@angular/http';

@Injectable()
export class TransferService {
  public add_subject = new Subject<String>()

  constructor(private http: Http) {
  }


  getUsers() {
    return this.http.get('/api/getAllUsers');
  }

}
