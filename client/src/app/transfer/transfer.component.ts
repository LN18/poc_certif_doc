import {Component, OnInit} from '@angular/core';
import {TransferService} from "./transfer.service";
import {GreenWalletService} from "../../services/green-wallet.service";
import {TypeaheadMatch} from "ngx-bootstrap";


@Component({
  selector: 'app-register',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})


export class TransferComponent implements OnInit {

  selected;


  amountCredit;
  amountDiscount;
  amount;

  beneficiaries;
  beneficiary;
  web3: any;
  credited = false;
  discounted = false;
  transfered = false;


  constructor(private transferService: TransferService,
              private greenWalletService: GreenWalletService) {

  }


  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.beneficiary = this.beneficiaries.find(beneficiary => beneficiary.credentials.username === e.value);
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers() {
    let identifier = localStorage.getItem('identifier');
    this.transferService.getUsers().subscribe(res => {
      this.beneficiaries = res.json().data.filter(beneficiary => beneficiary.credentials.username !== 'admin' && beneficiary.credentials.identifier !== identifier);
    })
  }


  submitForm() {

    let identifier = localStorage.getItem('identifier');
    this.greenWalletService.transfer(identifier, this.beneficiary.credentials.identifier, this.amount).subscribe(res => {
      this.transfered = true;
    });
  }


  credit() {
    let identifier = localStorage.getItem('identifier');
    this.greenWalletService.credit(identifier, this.amountCredit).subscribe(res => {
      this.credited = true;

    });
  }

  discount() {
    let identifier = localStorage.getItem('identifier');
    this.greenWalletService.discount(identifier, this.amountDiscount).subscribe(res => {
      this.discounted = true;
    });
  }
}
