import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { DragDropDirectiveModule} from "angular4-drag-drop";


import {AppRoutingModule} from './app.routing';
import {AppComponent} from './app.component';

import {ComponentsModule} from './components/components.module';

import {HomeComponent} from './home/home.component';
import {AuthComponent} from "./auth/auth.component";
import {FilesComponent} from './files/files.component';
import {ExitComponent} from './exit/exit.component';

import {UserProfileComponent} from './user-profile/user-profile.component';
import {TableListComponent} from './table-list/smart-meters/table-list.component';
import {TypographyComponent} from './typography/typography.component';
import {IconsComponent} from './icons/icons.component';
import {MapsComponent} from './maps/maps.component';
import {NotificationsComponent} from './notifications/notifications.component';
import {UpgradeComponent} from './upgrade/upgrade.component';
import {RegisterComponent} from "./auth/register.component";
import {Web3Service} from "../services/web3.service";
import {GreenWalletService} from "../services/green-wallet.service";
import {AuthentificationService} from "./auth/authentification.service";
import {TransferComponent} from "./transfer/transfer.component";
import {TransferService} from "./transfer/transfer.service";
import {MetersService} from "./table-list/meters.service";
import {AdminComponent} from "./table-list/admin/admin.component";


const SERVICES = [
  Web3Service,
  GreenWalletService,
  AuthentificationService,
  TransferService,
  MetersService,
]

@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
    HomeComponent,
    ExitComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    AuthComponent,
    RegisterComponent,
    TransferComponent,
    AdminComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    TypeaheadModule.forRoot(),
  ],
  providers: [SERVICES,],
  bootstrap: [AppComponent]
})
export class AppModule {
}
