import {Component, NgZone, OnInit} from '@angular/core';
import {Md5} from 'ts-md5/dist/md5';
import {Web3Service} from "../../services/web3.service";
import {GreenWalletService} from "../../services/green-wallet.service";
import {AuthentificationService} from "./authentification.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit{
  username;
  password;
  account: any;
  accounts: any;
  registred=false;
  variableToBeStored;

  constructor(private _ngZone: NgZone,
              private web3Service: Web3Service,
              private greenTransferService: GreenWalletService,
              private authenticationService:AuthentificationService) {

  }

  ngOnInit() {
    // Get the initial account balance so it can be displayed.
    this.web3Service.getAccounts().subscribe(accs => {
      this.accounts = accs;
      this.account = this.accounts[0];
      // This is run from window:load and ZoneJS is not aware of it we
      // need to use _ngZone.run() so that the UI updates on promise resolution
    }, err => alert(err))
  };

  createWallet(identifier) {

    let id = JSON.stringify(identifier);
    this.greenTransferService.createWallet(id.substring(1,id.length-1), this.account).subscribe(accs => {
    }, err => alert(err))
  }

  submitForm() {
    this.variableToBeStored = Md5.hashStr(this.username + this.password);
    console.log(this.variableToBeStored);
    this.createWallet(this.variableToBeStored);
    this.authenticationService.addCredentials({"identifier":this.variableToBeStored,"username":this.username}).subscribe(success=>
    {
      this.registred=true;
    });
  }
}
