import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Http } from '@angular/http';
import {Observable} from 'rxjs';

@Injectable()
export class AuthentificationService {
  public add_subject=new Subject<String>()

  constructor(private http : Http){
  }

  addCredentials(credentials){
    return this.http.post('/api/addCredentials',{
      credentials : credentials
    })
  }

  getCredentials(query){
    return this.http.post('/api/getCredentials',{query})
  }
}
