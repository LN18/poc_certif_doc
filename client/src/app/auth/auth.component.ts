import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthentificationService} from "./authentification.service";
import {Md5} from "ts-md5";

@Component({
    selector: 'auth-page',
    templateUrl: './auth.component.html'
})
export class AuthComponent {
    username;
    password;

    constructor(private router: Router,private authenticationService:AuthentificationService) {
    }

    signIn() {
        this.router.navigate(['/register']);
    }

    submitForm() {
      let identifier = Md5.hashStr(this.username + this.password);
      this.authenticationService.getCredentials({credentials: {identifier:identifier,username:this.username}}).subscribe(result=>{
        if(result.status==200){
          let id = JSON.stringify(identifier);
          localStorage.setItem('identifier', id.substring(1,id.length-1));
          if(this.username==='admin'){
            this.router.navigate(['/admin']);
          }else{
            this.router.navigate(['/dashboard']);
          }
        }
      })

    }
}
