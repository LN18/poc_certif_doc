import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Http} from '@angular/http';

@Injectable()
export class MetersService {
  public add_subject = new Subject<String>()

  constructor(private http: Http) {
  }

  addMeter(meter) {
    return this.http.post('/api/addMeter', {
      meter: meter
    })
  }

  getAllMeters() {
    return this.http.get('/api/getAllMeters');
  }

  updateMeter(meter) {
    return this.http.post('/api/updateMeter', {
      query: meter
    })
  }
}
