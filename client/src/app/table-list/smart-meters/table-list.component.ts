import {Component, OnInit} from '@angular/core';
import {MetersService} from "../meters.service";

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {

  meter = {name: "", location: "", date: "", state: false};
  meters: any;
  registred = false;

  constructor(private meterService: MetersService) {
  }

  ngOnInit() {
    this.getAllMeters();
  }

  getAllMeters() {
    this.meterService.getAllMeters().subscribe(res => {
      this.meters = res.json().data;
    });
  }

  addMeter() {

    this.meter.date = new Date().toDateString();
    this.meterService.addMeter(this.meter).subscribe(success => {
      this.registred = true;
      this.getAllMeters();
    });
  }
}
