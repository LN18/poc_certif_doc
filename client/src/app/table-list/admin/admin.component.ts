import {Component, OnInit} from '@angular/core';
import {MetersService} from "../meters.service";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  meter = {name: "", location: "", date: "", state: false};
  meters: any;
  registred = false;

  constructor(private meterService: MetersService) {
  }

  ngOnInit() {
    this.getAllMeters();
  }

  getAllMeters() {
    this.meterService.getAllMeters().subscribe(res => {
      this.meters = res.json().data;
      console.log(this.meters);
    });
  }

  enableMeter(_id) {
    let query;
    query = this.meters.find(meter => _id === meter._id);
    query.meter.state=true;
    this.meterService.updateMeter(query).subscribe();
  }

  disableMeter(_id) {
    let query;
    query = this.meters.find(meter => _id === meter._id);
    query.meter.state=false;
    this.meterService.updateMeter(query).subscribe();
  }
}
