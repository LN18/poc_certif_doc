import {Component, OnInit} from '@angular/core';
import * as Chartist from 'chartist';
import {GreenWalletService} from "../../services/green-wallet.service";

import {sha3_256} from 'js-sha3';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {
  balance: any;
  wallet: any;
  web3;
  found;
  notfound;

  constructor(private greenTransferService: GreenWalletService,) {

  }

  getBalance() {
    this.greenTransferService.getBalance().subscribe(accs => {
      this.balance = accs[1].toNumber();
      console.log(accs[1]);
      console.log(accs[0]);


    });
  }


  ngOnInit() {
    this.getBalance();
    /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */


  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];

      reader.readAsArrayBuffer(file);
      reader.onload = () => {
        let hash = sha3_256(reader.result);
        this.greenTransferService.saveDocumentHash(hash).subscribe();
      };
    }
  }

  onFileChangeFind(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];

      reader.readAsArrayBuffer(file);
      reader.onload = () => {
        let hash = sha3_256(reader.result);
        console.log(hash);
        this.greenTransferService.getHashes().subscribe(result => console.log(result));
        this.greenTransferService.findDocumentHash(hash).subscribe(result => {
            if (result) {
              this.found = true;
            }
            this.notfound = true;
          }
        );
      };
    }
  }
}
