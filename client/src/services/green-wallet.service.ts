import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Web3Service} from './web3.service'
import {publicNetwork} from "../environments/environment.dev";

const greenWalletArtifacts = require('../../build/contracts/GreenWallet.json');
const contract = require('truffle-contract');
var BigNumber = require('big-number');
const Web3 = require('web3');
@Injectable()
export class GreenWalletService implements OnInit {

  GreenWallet = contract(greenWalletArtifacts);
  account: any;
  web3:any;

  constructor(private web3Ser: Web3Service) {
    // Bootstrap the GreenWallet abstraction for Use
    this.GreenWallet.setProvider(web3Ser.web3.currentProvider);
    this.web3Ser.getAccounts().subscribe(accs => {
      console.log("accounts----" + accs);
      this.account = accs[0];
      // This is run from window:load and ZoneJS is not aware of it we
      // need to use _ngZone.run() so that the UI updates on promise resolution
    }, err => alert(err))
  }

  ngOnInit() {
  }

  createWallet(identifier, account): Observable<any> {
    let wallet;
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          wallet = instance;
          return wallet.createWallet(identifier, {
            from: account,
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getBalance(): Observable<any> {
    let meta;
    let identifier = localStorage.getItem('identifier');
    console.log(identifier);
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getCoinBalance.call(identifier, {
            from: this.account,
          });
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getBalanceById(identifier): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getCoinBalance.call(identifier, {
            from: this.account,
          });
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getOneWallet(): Observable<any> {
    let meta;
    let identifier = localStorage.getItem('identifier').toString();

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getOneWallet.call(identifier, {
            from: this.account,
          });
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getWallets(): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getWallets.call();
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getIdentifiers(): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getIdentifiers.call();
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }


  transfer(identifier, toIdentifier, amount) {
    let meta;
    console.log(amount);
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.transfer(identifier, toIdentifier, amount, {
            from: this.account
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          console.log(e);
          observer.error(e)
        });
    })

  }

  getTransfers(): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getTransfers.call();
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  credit(identifier, amount) {
    let meta;
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          console.log(amount);
          return meta.credit(identifier, parseInt(amount), {
            from: this.account
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          console.log(e);
          observer.error(e)
        });
    })
  }

  discount(identifier, amount) {
    let meta;
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          console.log(amount);
          return meta.getDiscount(identifier, parseInt(amount), {
            from: this.account
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          console.log(e);
          observer.error(e)
        });
    })
  }

  saveDocumentHash(hash: string) {
    let meta;
    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.saveDocumentHash(hash,{
            from: this.account
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          console.log(e);
          observer.error(e)
        });
    })
  }

  findDocumentHash(hash: string) {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getDocumentByHash.call(hash, {
            from: this.account,
          });
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getHashes(): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.GreenWallet
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getHashes.call();
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }
}
