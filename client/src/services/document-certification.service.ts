import {Injectable, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Web3Service} from './web3.service'
import {publicNetwork} from "../environments/environment.dev";

const documentCertificationArtifacts = require('../../build/contracts/DocumentCertification.json');
const contract = require('truffle-contract');

@Injectable()
export class DocumentCertificationService implements OnInit {

  documentCertification = contract(documentCertificationArtifacts);
  account: any;
  web3:any;

  constructor(private web3Ser: Web3Service) {
    // Bootstrap the GreenWallet abstraction for Use
    this.documentCertification.setProvider(web3Ser.web3.currentProvider);
    this.web3Ser.getAccounts().subscribe(accs => {
      console.log("accounts----" + accs);
      this.account = accs[0];
      // This is run from window:load and ZoneJS is not aware of it we
      // need to use _ngZone.run() so that the UI updates on promise resolution
    }, err => alert(err))
  }

  ngOnInit() {
  }



  saveDocumentHash(hash: string) {
    let meta;
    return Observable.create(observer => {
      this.documentCertification
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.saveDocumentHash(hash,{
            from: this.account
          });
        })
        .then(() => {
          observer.next()
          observer.complete()
        })
        .catch(e => {
          console.log(e);
          observer.error(e)
        });
    })
  }

  findDocumentHash(hash: string) {
    let meta;

    return Observable.create(observer => {
      this.documentCertification
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getDocumentByHash.call(hash, {
            from: this.account,
          });
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }

  getHashes(): Observable<any> {
    let meta;

    return Observable.create(observer => {
      this.documentCertification
        .deployed()
        .then(instance => {
          meta = instance;
          return meta.getHashes.call();
        })
        .then(value => {
          observer.next(value)
          observer.complete()
        })
        .catch(e => {
          observer.error(e)
        });
    })
  }
}
