import {Web3Service} from './web3.service'
import {GreenWalletService} from './green-wallet.service';
import {DocumentCertificationService} from "./document-certification.service";


export {
  Web3Service,
  GreenWalletService,
  DocumentCertificationService,
};
