var GreenWallet = artifacts.require("GreenWallet");
var Wallet = artifacts.require("Wallet");
var ConvertLib = artifacts.require("./ConvertLib.sol");
var DocumentCertification = artifacts.require("./DocumentCertification.sol");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, GreenWallet);
  deployer.deploy(GreenWallet);
  deployer.deploy(Wallet);
  deployer.deploy(DocumentCertification);
};
